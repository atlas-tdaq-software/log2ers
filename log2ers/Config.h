#ifndef LOG2ERS_CONFIG_H
#define LOG2ERS_CONFIG_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace log2ers {
class DataSink;
}

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class which configures log2ers
 *
 *  Configuration is read from OKS object of type Log2ERSConfig with ID
 *  given in the TDAQ_APPLICATION_NAME envvar. It also needs TDAQ_PARTITION
 *  and TDAQ_DB envars to be set to function correctly.
 *
 *  @author Andy Salnikov
 */

class Config  {
public:

    // Default constructor
    Config();

    // Destructor
    ~Config();

    /**
     *  Return the list of data sinks for standard output.
     *
     *  Returned list should never be empty.
     */
    std::vector<std::shared_ptr<DataSink>> const& stdout_sinks() const { return m_stdout_sinks; }

    /**
     *  Return the list of data sinks for standard error.
     *
     *  Returned list should never be empty.
     */
    std::vector<std::shared_ptr<DataSink>> const& stderr_sinks() const { return m_stderr_sinks; }

    /**
     *  Return default list of data sinks for standard output (without ERS).
     *
     *  This does not check configuration, simply returns sink wich send stuff to stdout.
     *  Returned list should never be empty.
     */
    static std::vector<std::shared_ptr<DataSink>> def_stdout_sinks();

    /**
     *  Return default list of data sinks for standard error (without ERS).
     *
     *  This does not check configuration, simply returns sink which send stuff to stderr.
     *  Returned list should never be empty.
     */
    static std::vector<std::shared_ptr<DataSink>> def_stderr_sinks();

protected:

private:

    std::vector<std::shared_ptr<DataSink>> m_stdout_sinks;
    std::vector<std::shared_ptr<DataSink>> m_stderr_sinks;
};

} // namespace log2ers

#endif // LOG2ERS_CONFIG_H
