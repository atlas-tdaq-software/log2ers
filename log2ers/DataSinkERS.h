#ifndef LOG2ERS_DATASINKERS_H
#define LOG2ERS_DATASINKERS_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "log2ers/DataSink.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace log2ers {
class MessageParser;
}

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class implementing DataSink interface for sending messages to ERS.
 *
 *  @author Andy Salnikov
 */

class DataSinkERS : public DataSink {
public:

    // Constructors
    explicit DataSinkERS(std::vector<MessageParser> const& parsers);
    explicit DataSinkERS(std::vector<MessageParser>&& parsers);

    DataSinkERS(DataSinkERS const&) = delete;
    DataSinkERS& operator=(DataSinkERS const&) = delete;

    // Destructor will close file descriptor
    virtual ~DataSinkERS();

    /**
     *  Send bunch of bytes to their destination.
     *
     *  Can throw in case of errors.
     */
    virtual void write(const char* buf, size_t size) override;

protected:

    /// Process single line message, this does all filtering and substitution
    virtual void process(std::string const& line);

private:

    std::string m_current_line;
    std::vector<MessageParser> m_parsers;
};

} // namespace log2ers

#endif // LOG2ERS_DATASINKERS_H
