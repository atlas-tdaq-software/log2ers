#ifndef LOG2ERS_DATASINKFD_H
#define LOG2ERS_DATASINKFD_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "log2ers/DataSink.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class implementing DataSink interface for destination which is a file descriptor.
 *
 *  @author Andy Salnikov
 */

class DataSinkFD : public DataSink {
public:

    /// Constructor takes file descriptor
    explicit DataSinkFD(int dst_fd) : m_dst_fd(dst_fd) {}

    DataSinkFD(DataSinkFD const&)= delete;
    DataSinkFD& operator=(DataSinkFD const&)= delete;

    // Destructor
    virtual ~DataSinkFD();

    /**
     *  Send bunch of bytes to their destination.
     *
     *  Can throw in case of errors.
     */
    virtual void write(const char* buf, size_t size) override;

protected:

private:

    int m_dst_fd;

};

} // namespace log2ers

#endif // LOG2ERS_DATASINKFD_H
