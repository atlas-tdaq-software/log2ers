#ifndef LOG2ERS_DATASINK_H
#define LOG2ERS_DATASINK_H

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <cstddef>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class definit abstract intrface for "data sinks".
 *
 *
 *
 *  @author Andy Salnikov
 */

class DataSink {
public:

    DataSink(DataSink const&) = delete;
    DataSink& operator=(DataSink const&) = delete;

    // Destructor
    virtual ~DataSink() {}

    /**
     *  Send bunch of bytes to their destination.
     *
     *  Can throw in case of errors.
     */
    virtual void write(const char* buf, size_t size) = 0;

protected:

    DataSink() {}

private:

};

} // namespace log2ers

#endif // LOG2ERS_DATASINK_H
