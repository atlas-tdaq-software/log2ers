#ifndef LOG2ERS_MESSAGEPARSER_H
#define LOG2ERS_MESSAGEPARSER_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <regex>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class which parses input messages and converts them into issues.
 *
 *  Each message parser has an associated level (see level() method).
 *  Parser's method parse() takes a message (string), matches it
 *
 *  @author Andy Salnikov
 */

class MessageParser {
public:

    /**
     *  Constructor can throw.
     *
     *  @param severity: message severity associated with this parser
     *  @param message_re_str: string with regexp to match the whole message
     *  @param format_message: format string to extract relevant part of the message
     *  @param format_package: format string to extract package name
     *  @param format_file_name: format string to extract file name
     *  @param format_func_name: format string to extract function name
     *  @param format_line_no: format string to extract source line number
     */
    MessageParser(ers::severity severity,
                  std::string const& message_re_str,
                  std::string const& format_message,
                  std::string const& format_package,
                  std::string const& format_file_name,
                  std::string const& format_func_name,
                  std::string const& format_line_no);

    // Destructor
    ~MessageParser();

    /// Return severity associated with the parser
    ers::severity severity() const { return m_severity; }

    /**
     *  Parse input message and make ERS issue out of it.
     *
     *  If message does not parse it returns zero pointer. No exceptions
     *  throw by this method
     */
    std::unique_ptr<ers::Issue> parse(std::string const& message) const;

protected:

private:

    ers::severity m_severity;
    std::string m_message_re_str;
    std::string m_format_message;
    std::string m_format_package;
    std::string m_format_file_name;
    std::string m_format_func_name;
    std::string m_format_line_no;
    std::regex m_message_re;

    mutable std::string m_package;
    mutable std::string m_file_name;
    mutable std::string m_func_name;
};

} // namespace log2ers

#endif // LOG2ERS_MESSAGEPARSER_H
