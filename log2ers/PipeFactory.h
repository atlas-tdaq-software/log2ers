#ifndef LOG2ERS_PIPEFACTORY_H
#define LOG2ERS_PIPEFACTORY_H

//-----------------
// C/C++ Headers --
//-----------------
#include <utility>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class implementing management of file descriptors on both
 *  sides of a inter-process pipe.
 *
 *  The purpose of this class is to hide file descriptor juggling when we
 *  replace normal output destination with a pipe to a subprocess. It handles
 *  single output file descriptor and a single pipe, usually we redirect two
 *  file descriptors (stdout/stderr) so two instances of this class are needed.
 *
 *  Typical use case for this class:
 *  - make an instance before fork giving it FD to be redirected
 *  - after the fork one process calls writer_redirect() method
 *  - second process calls child_fds() method
 *
 *      PipeFactory pipe_stdout(1);
 *      auto pid = fork();
 *      if (pid == 0) {
 *          // child process
 *          auto fds_stdout = pipe_stdout.child_fds();
 *          // ... do something with fds_stdout
 *      } else if (pid > 0) {
 *          // parent process
 *          pipe_stdout.writer_redirect();
 *      }
 *
 *  @author Andy Salnikov
 */

class PipeFactory {
public:

    /**
     *  Create a pipe for redirection.
     *
     *  Constructor will not throw, instead if pipe construction fails it
     *  will print an error message to standard error. The redirect methods
     *  will not do anything then so all output will be sent to its original
     *  destination.
     *
     *  @param initial_fd:  file descriptor to redirect to a pipe
     */
    explicit PipeFactory(int initial_fd);

    /**
     *  Setup descriptors on writer side so that output directed to a pipe.
     *
     *  If pipe construction fails then this method does nothing. Otherwise it
     *  closes reading side of the pipe and assigns initial FD to a writing
     *  side of the pipe.
     */
    void writer_redirect();

    /**
     *  Setup descriptors on child side, making them available for reading
     *  and writing.
     *
     *  If pipe construction fails then this method does nothing and returns
     *  negative numbers. Otherwise it closes writing end of the pipe and
     *  returns pair of file descriptors, first one is pipe's reading end,
     *  second is original FD.
     */
    std::pair<int, int> child_fds();

protected:

private:

    int m_initial_fd;
    int m_pipe_fds[2];

};

} // namespace log2ers

#endif // LOG2ERS_PIPEFACTORY_H
