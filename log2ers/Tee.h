#ifndef LOG2ERS_TEE_H
#define LOG2ERS_TEE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace log2ers {
class DataSink;
}

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace log2ers {

/// @addtogroup log2ers

/**
 *  @ingroup log2ers
 *
 *  Class which reads data from file descriptor and sends it to multiple destinations.
 *
 *  @author Andy Salnikov
 */

class Tee {
public:

    /**
     *  Constructor takes input file descriptor and arbitrary number of
     *  objects representing outputs.
     */
    Tee(int src_fd, std::vector<std::shared_ptr<DataSink>> const& dst, unsigned buf_size=10*1024);

    // Destructor
    ~Tee();

    /**
     *  Run read loop.
     *
     *  This method returns only when there is no more data (when read from
     *  input pipe return zero bytes). On return it closes the file associated
     *  with input descriptor.
     */
    void operator()();

protected:

private:

    int m_src_fd;  // input pipe file descriptor.
    std::vector<std::shared_ptr<DataSink>> m_dst;
    unsigned m_buf_size;
};

} // namespace log2ers

#endif // LOG2ERS_TEE_H
