//--------------------------------------------------------------------------
// Description:
//	Test suite case for the log2ers::MessageParser.
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "log2ers/MessageParser.h"

using namespace log2ers;

#define BOOST_TEST_MODULE test_log2ers_message_parser
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

/**
 * Simple test suite for module coca_test_fs.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

// ==============================================================

BOOST_AUTO_TEST_CASE(test_sev)
{
    MessageParser mp1(ers::Debug, "", "", "", "", "", "");
    BOOST_CHECK_EQUAL(mp1.severity(), ers::Debug);
    MessageParser mp2(ers::Log, "", "", "", "", "", "");
    BOOST_CHECK_EQUAL(mp2.severity(), ers::Log);
    MessageParser mp3(ers::Error, "", "", "", "", "", "");
    BOOST_CHECK_EQUAL(mp3.severity(), ers::Error);
}

BOOST_AUTO_TEST_CASE(test_any)
{
    MessageParser mp1(ers::Debug, ".*", "", "", "", "", "");

    std::string message = "any message";
    auto issue1 = mp1.parse(message);
    BOOST_REQUIRE(issue1 != nullptr);
    BOOST_CHECK_EQUAL(issue1->message(), message);
    BOOST_CHECK_EQUAL(issue1->context().file_name(), "");
    BOOST_CHECK_EQUAL(issue1->context().function_name(), "");
    BOOST_CHECK_EQUAL(issue1->context().line_number(), 0);
    BOOST_CHECK_EQUAL(issue1->context().package_name(), "");

    MessageParser mp2(ers::Debug, ".*", "$&", "", "", "", "");
    auto issue2 = mp2.parse(message);
    BOOST_REQUIRE(issue2 != nullptr);
    BOOST_CHECK_EQUAL(issue2->message(), message);
}

BOOST_AUTO_TEST_CASE(test_match)
{
    MessageParser mp1(ers::Error, "ERROR (.*)", "", "", "", "", "");

    std::string message = "no match";
    auto issue1 = mp1.parse(message);
    BOOST_CHECK(issue1 == nullptr);

    message = "ERROR this will match";
    issue1 = mp1.parse(message);
    BOOST_REQUIRE(issue1 != nullptr);
    BOOST_CHECK_EQUAL(issue1->message(), message);
    BOOST_CHECK_EQUAL(issue1->context().file_name(), "");
    BOOST_CHECK_EQUAL(issue1->context().function_name(), "");
    BOOST_CHECK_EQUAL(issue1->context().line_number(), 0);
    BOOST_CHECK_EQUAL(issue1->context().package_name(), "");

    MessageParser mp2(ers::Error, "ERROR (.*)", "$1", "", "", "", "");
    auto issue2 = mp2.parse(message);
    BOOST_REQUIRE(issue2 != nullptr);
    BOOST_CHECK_EQUAL(issue2->message(), "this will match");
}

BOOST_AUTO_TEST_CASE(test_names)
{
    MessageParser mp1(ers::Error, "ERROR \\[(.*) at (.*):(\\d+)\\] (.*)", "$4", "package", "$2", "$1", "$3");

    std::string message = "ERROR [void function(...) at ./filename.cxx:123] error message";
    auto issue1 = mp1.parse(message);
    BOOST_REQUIRE(issue1 != nullptr);
    BOOST_CHECK_EQUAL(issue1->message(), "error message");
    BOOST_CHECK_EQUAL(issue1->context().file_name(), "./filename.cxx");
    BOOST_CHECK_EQUAL(issue1->context().function_name(), "void function(...)");
    BOOST_CHECK_EQUAL(issue1->context().line_number(), 123);
    BOOST_CHECK_EQUAL(issue1->context().package_name(), "package");
}

BOOST_AUTO_TEST_CASE(test_timestamp)
{
    MessageParser mp1(ers::Error, "(\\d{4}-[A-Za-z]{3}-\\d\\d [0-9:,]+) ERROR (.*)", "$2 (original time: $1)", "", "", "", "");

    std::string message = "2016-Nov-01 10:50:07,786 ERROR error message";
    auto issue1 = mp1.parse(message);
    BOOST_REQUIRE(issue1 != nullptr);
    BOOST_CHECK_EQUAL(issue1->message(), "error message (original time: 2016-Nov-01 10:50:07,786)");
}
