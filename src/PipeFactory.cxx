//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/PipeFactory.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cstdio>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//----------------
// Constructors --
//----------------
PipeFactory::PipeFactory(int initial_fd)
    : m_initial_fd(initial_fd)
{
    // make pipe
    int st = pipe(m_pipe_fds);
    if (st < 0) {
        perror("log2ers - failed to make a pipe");
        m_pipe_fds[0] = -1;
        m_pipe_fds[1] = -1;
    }
}

void
PipeFactory::writer_redirect()
{
    // If pipe() failed then do nothing
    if (m_pipe_fds[0] < 0) return;

    // Assign original FD to writing end of a pipe.
    // This may fail but hopefully it should result in original FD still pointing
    // to original location, and it's still OK to close both ends of pipe.
    if (dup2(m_pipe_fds[1], m_initial_fd) < 0) {
        perror("log2ers - dup2 failed");
    }

    // close all other FDs
    close(m_pipe_fds[0]);
    close(m_pipe_fds[1]);
}

std::pair<int, int>
PipeFactory::child_fds()
{
    // If pipe() failed then do nothing
    if (m_pipe_fds[0] < 0) return std::make_pair(-1, -1);

    // close writing end of pipe
    close(m_pipe_fds[1]);

    return std::make_pair(m_pipe_fds[0], m_initial_fd);
}

} // namespace log2ers
