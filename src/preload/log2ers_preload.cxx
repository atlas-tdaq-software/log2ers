//----------------------------------------------------------------------
// File and Version Information:
//  Source code for share
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <cstdio>
#include <string>
#include <unistd.h>
#include <thread>
#include <sys/types.h>
#include <sys/wait.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "log2ers/Config.h"
#include "log2ers/PipeFactory.h"
#include "log2ers/Tee.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(errors, ConfigurationFailed,
                  "Failed to configure ers2log, there will be no ERS message forwarding", )

/*
 *  Class to implement RAII idiom to control log2ers child process.
 *
 *  Forks a subprocess in constructor which runs log2ers filters.
 *  In destructor makes sure that subprocess stops before this process
 *  so that all output is finished before we exit (see ADHI-4362 for
 *  motivation).
 */
class SubprocessGuard {
public:

    SubprocessGuard() : m_subproc(_init_log2ers()) {}

    ~SubprocessGuard() {
        if (m_subproc != 0) {
            // wait for child process to stop, for that we have to close pipe ends on our side
            close(1);
            close(2);
            waitpid(m_subproc, nullptr, 0);
        }
    }

private:

    pid_t _init_log2ers() {

        // make pipes
        log2ers::PipeFactory pipe_out(1);
        log2ers::PipeFactory pipe_err(2);

        // start sub-process to run log2ers
        auto pid = fork();
        if (pid < 0) {
            perror("log2ers - fork failed");
            return 0;
        } else if (pid == 0) {
            // child process will run piper
            auto fds_out = pipe_out.child_fds();
            auto fds_err = pipe_err.child_fds();

            auto stdout_sinks = log2ers::Config::def_stdout_sinks();
            auto stderr_sinks = log2ers::Config::def_stderr_sinks();
            std::unique_ptr<log2ers::Config> config;
            try {
                log2ers::Config config;
                stdout_sinks = config.stdout_sinks();
                stderr_sinks = config.stderr_sinks();
            } catch (std::exception const& exc) {
                ers::warning(errors::ConfigurationFailed(ERS_HERE, exc));
            }

            std::thread out_thread(log2ers::Tee(fds_out.first, stdout_sinks));
            std::thread err_thread(log2ers::Tee(fds_err.first, stderr_sinks));
            out_thread.join();
            err_thread.join();

            exit(0);
        } else {
            // parent process - close extra FDs
            pipe_out.writer_redirect();
            pipe_err.writer_redirect();
        }
        return pid;
    }

private:
    pid_t m_subproc;  // Child process PID
};

// global instance which is constructed on shared object initialization
// and destroyed when shared object is finalized
SubprocessGuard _guard;

}
