//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/MessageParser.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//----------------
// Constructors --
//----------------
MessageParser::MessageParser(ers::severity severity,
                             std::string const& message_re_str,
                             std::string const& format_message,
                             std::string const& format_package,
                             std::string const& format_file_name,
                             std::string const& format_func_name,
                             std::string const& format_line_no)
    : m_severity(severity),
      m_message_re_str(message_re_str),
      m_format_message(format_message),
      m_format_package(format_package),
      m_format_file_name(format_file_name),
      m_format_func_name(format_func_name),
      m_format_line_no(format_line_no),
      m_message_re(message_re_str)
{
}

//--------------
// Destructor --
//--------------
MessageParser::~MessageParser()
{
}

std::unique_ptr<ers::Issue>
MessageParser::parse(std::string const& message) const
{
    std::unique_ptr<ers::Issue> result(nullptr);

    // parse message
    std::smatch match;
    try {
        if (not std::regex_match(message, match, m_message_re)) {
            return result;
        }
    } catch (std::exception const& exc) {
        // Do not make much noise about it
        ERS_LOG("Failed while parsing message \"" << message << "\"\n"
                " -- regexp: \"" << m_message_re_str << "\"\n"
                " -- error: \"" << exc.what() << "\"");
        return result;
    }

    // extract various pieces out of this match
    std::string logged_message;
    m_package.erase();
    m_file_name.erase();
    m_func_name.erase();
    int line_no = 0;
    try {
        logged_message = match.format(m_format_message);
        m_package = match.format(m_format_package);
        m_file_name = match.format(m_format_file_name);
        m_func_name = match.format(m_format_func_name);
        std::string line_no_str = match.format(m_format_line_no);
        if (not line_no_str.empty()) {
            try {
                line_no = std::stoi(line_no_str);
            } catch(std::exception const& exc) {
                // Don't even want to complain here
            }
        }
    } catch (std::exception const& exc) {
        // Do not make much noise about it
        ERS_LOG("Failed while formatting \"" << message << "\"\n"
                " -- regexp: \"" << m_message_re_str << "\"\n"
                " -- error: \"" << exc.what() << "\"");
    }

    // message format is empty take whole original message
    if (logged_message.empty()) {
        logged_message = message;
    }
    if (not logged_message.empty()) {
        ers::LocalContext context(m_package.c_str(), m_file_name.c_str(), line_no, m_func_name.c_str());
        result.reset(new ers::Message(context, logged_message));
    }

    return result;
}

} // namespace log2ers
