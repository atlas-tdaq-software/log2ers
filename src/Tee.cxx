//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/Tee.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cerrno>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "log2ers/DataSink.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//----------------
// Constructors --
//----------------
Tee::Tee(int src_fd, std::vector<std::shared_ptr<DataSink>> const& dst, unsigned buf_size)
    : m_src_fd(src_fd), m_dst(dst), m_buf_size(buf_size)
{
}

Tee::~Tee()
{
}

void
Tee::operator()()
{
    char* buf = new char[m_buf_size];
    while(true) {
        auto bytes_read = read(m_src_fd, buf, m_buf_size);
        if (bytes_read == 0) {
            // other end closed
            break;
        } else if (bytes_read < 0) {
            if (errno != EINTR and errno != EAGAIN) {
                break;
            }
        } else {
            // copy to all destinations
            for (auto&& dst: m_dst) {
                dst->write(buf, size_t(bytes_read));
            }
        }
    }
    close(m_src_fd);
}

} // namespace log2ers
