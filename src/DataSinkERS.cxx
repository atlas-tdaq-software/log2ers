//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/DataSinkERS.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "log2ers/MessageParser.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//----------------
// Constructors --
//----------------
DataSinkERS::DataSinkERS(std::vector<MessageParser> const& parsers)
    : DataSink(),
      m_current_line(),
      m_parsers(parsers)
{
}

DataSinkERS::DataSinkERS(std::vector<MessageParser>&& parsers)
    : DataSink(),
      m_current_line(),
      m_parsers(std::move(parsers))
{
}

//--------------
// Destructor --
//--------------
DataSinkERS::~DataSinkERS()
{
}

void
DataSinkERS::write(const char* buf, size_t size)
{
    // split into lines at newline, process each line separately
    for (const char *p = buf, *end = buf+size; p != end; ++ p) {
        if (*p == '\n') {
            // send next line
            process(m_current_line);
            m_current_line.erase();
        } else {
            m_current_line.push_back(*p);
        }
    }
}

void
DataSinkERS::process(std::string const& line)
{
    // ignore empty lines
    if (line.empty()) return;

    // call each parser in turn to match this line
    for (auto&& parser: m_parsers) {
        auto issue = parser.parse(line);
        if (issue != nullptr) {
            switch(parser.severity()) {
            case ers::Debug:
                ers::debug(*issue);
                break;
            case ers::Log:
                ers::log(*issue);
                break;
            case ers::Information:
                ers::info(*issue);
                break;
            case ers::Warning:
                ers::warning(*issue);
                break;
            case ers::Error:
                ers::error(*issue);
                break;
            case ers::Fatal:
                ers::fatal(*issue);
                break;
            }
            // stop here, the first parser wins
            break;
        }
    }
}

} // namespace log2ers
