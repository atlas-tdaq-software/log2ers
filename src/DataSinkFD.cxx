//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/DataSinkFD.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cerrno>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//--------------
// Destructor --
//--------------
DataSinkFD::~DataSinkFD()
{
}

void DataSinkFD::write(const char* buf, size_t size)
{
    while (size > 0) {
        auto bytes = ::write(m_dst_fd, buf, size);
        if (bytes < 0) {
            if (errno != EINTR and errno != EAGAIN) {
                // TODO: should probably complain here
                break;
            }
        } else {
            size -= bytes;
            buf += bytes;
        }
    }
}

} // namespace log2ers
