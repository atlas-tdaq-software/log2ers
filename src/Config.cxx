//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "log2ers/Config.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "config/Configuration.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "log2ers/DataSinkERS.h"
#include "log2ers/DataSinkFD.h"
#include "log2ers/Log2ERSConfig.h"
#include "log2ers/Log2ERSLineParse.h"
#include "log2ers/MessageParser.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(errors, MissingEnvironment,
                  "Environment variable " << name << " is not defined",
                  ((std::string)name))

ERS_DECLARE_ISSUE(errors, FailedLoadDB,
                  "Failed to load configuration database " << tdaq_db,
                  ((std::string)tdaq_db))

ERS_DECLARE_ISSUE(errors, MissingConfiguration,
                  "Configuration object " << id << "@" << klass << " is not found",
                  ((std::string)id) ((std::string)klass))

ERS_DECLARE_ISSUE(errors, ConfigurationError,
                  "Error in configuration object " << name << ", object will be ignored",
                  ((std::string)name))

// convert Log2ERSLineParse into MessageParser, can throw
log2ers::MessageParser
make_parser(log2ers::Log2ERSLineParse const& pconf)
{
    ers::severity sev;
    ers::parse(pconf.get_Severity(), sev);
    return log2ers::MessageParser(sev,
                                  pconf.get_Regexp(),
                                  pconf.get_Message(),
                                  pconf.get_Package(),
                                  pconf.get_FileName(),
                                  pconf.get_FunctionName(),
                                  pconf.get_LineNumber());
}

std::vector<log2ers::MessageParser>
make_parsers(std::vector<const log2ers::Log2ERSLineParse*> const& configs) {
    std::vector<log2ers::MessageParser> parsers;
    for (auto&& pconf: configs) {
        try {
            parsers.push_back(::make_parser(*pconf));
        } catch (std::exception const& exc) {
            ers::log(errors::ConfigurationError(ERS_HERE, pconf->full_name(), exc));
        }
    }
    return parsers;
}

}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace log2ers {

//----------------
// Constructors --
//----------------
Config::Config()
{
    // initialize IPC
    int argc = 0;
    IPCCore::init(argc, nullptr);

    // TDAQ_DB must be set
    const char* tdaq_db = getenv("TDAQ_DB");
    if (tdaq_db == nullptr or tdaq_db[0] == '\0') {
        throw ::errors::MissingEnvironment(ERS_HERE, "TDAQ_DB");
    }

    // load configuration database
    std::unique_ptr<Configuration> confdb(nullptr);
    try {
        confdb.reset(new Configuration(tdaq_db));
        if (not confdb->loaded()) {
            throw ::errors::FailedLoadDB(ERS_HERE, tdaq_db);
        }
    } catch (std::exception const& exc) {
        throw ::errors::FailedLoadDB(ERS_HERE, tdaq_db, exc);
    }

    // get configuration name from either LOG2ERS_CONFIG_NAME or TDAQ_APPLICATION_NAME
    const char* config_name = getenv("LOG2ERS_CONFIG_NAME");
    if (config_name == nullptr or config_name[0] == '\0') {
        config_name = getenv("TDAQ_APPLICATION_NAME");
        if (config_name == nullptr or config_name[0] == '\0') {
            throw ::errors::MissingEnvironment(ERS_HERE, "LOG2ERS_CONFIG_NAME or TDAQ_APPLICATION_NAME");
        }
    }

    // find configuration object
    const Log2ERSConfig *conf = confdb->get<Log2ERSConfig>(config_name, true);
    if (conf == nullptr) {
        throw ::errors::MissingConfiguration(ERS_HERE, config_name, "Log2ERSConfig");
    }

    m_stdout_sinks.push_back(std::make_shared<log2ers::DataSinkFD>(1));
    std::vector<log2ers::MessageParser> parsers = ::make_parsers(conf->get_StdoutParsers());
    if (not parsers.empty()) {
        m_stdout_sinks.push_back(std::make_shared<log2ers::DataSinkERS>(parsers));
    }

    m_stderr_sinks.push_back(std::make_shared<log2ers::DataSinkFD>(2));
    parsers = ::make_parsers(conf->get_StderrParsers());
    if (not parsers.empty()) {
        m_stderr_sinks.push_back(std::make_shared<log2ers::DataSinkERS>(parsers));
    }
}

//--------------
// Destructor --
//--------------
Config::~Config()
{
}

std::vector<std::shared_ptr<DataSink>>
Config::def_stdout_sinks()
{
    std::vector<std::shared_ptr<DataSink>> sinks;
    sinks.push_back(std::make_shared<log2ers::DataSinkFD>(1));
    return sinks;
}

std::vector<std::shared_ptr<DataSink>>
Config::def_stderr_sinks()
{
    std::vector<std::shared_ptr<DataSink>> sinks;
    sinks.push_back(std::make_shared<log2ers::DataSinkFD>(2));
    return sinks;
}

} // namespace log2ers
