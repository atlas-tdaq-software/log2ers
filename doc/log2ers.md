Forwarding log messages to ERS with log2ers
===========================================

ATLAS TDAQ uses some third-party applications which report error conditions
via standard means - logging to standard output or error streams. ATLAS TDAQ
relies on ERS for message reporting and applications that do not use ERS
require special attention from experts to recognize error conditions for
those applications. It would be beneficial to integrate those third-party
applications with the the rest of TDAQ and make them report their errors to
ERS. Because modification of those applications is not possible an external
solution is needed for that. `log2ers` implements such solution which is
based on watching and filtering application output.

Basic principles of operation
-----------------------------

`log2ers` works by watching standard error and standard output produced by
application. It splits output streams into individual lines separated by
newline character and matches those lines against a set of configured regular
expressions. When line matches one of regular expressions `log2ers` extracts
few pieces of information from the line, makes a ERS message out of those
pieces and sends that message to a corresponding ERS stream (e.g. ERROR
stream).

`log2ers` is implemented as a shared library which has to be preloaded using
`LD_PRELOAD` mechanism of the dynamic loader. This allows us to plug it into
TDAQ configuration with minimal changes to existing application configuration.
An example of the command line use of this library would be (ignoring
configuration issues for now):

    $ LD_PRELOAD=liblog2ers_preload.so coralServer ...

To watch application output `log2ers` transparently replaces application
standard output and standard error with pipes and forks a sub-process which
reads those pipes and analyses application output. Sub-process takes care of
re-sending unmodified output to its original destination (file or terminal)
and also forwards interesting messages to ERS.

Implementation details
----------------------

All  work for setting up `log2ers` is done during initialization of the
shared library and this includes following steps:
  * `log2ers` loads its configuration from a special configuration class
    using regular TDAQ `config` facility
  * it creates two pipes for sending application output from main process to
    a child process
  * it forks the process
  * in a child process it starts two threads to read from reading ends of the
    pipes and waits for these threads to finish
  * in a parent process it redirects file descriptors 1 and 2 to writing ends
    of these pipes and proceeds to running original application, when
    application finishes writing ends of pipes are closed
  * in a child process threads read and match application output against
    configured regular expressions and send matching output as messages to ERS
  * child threads also copy unmodified application output to its original
    destination (file descriptors 1 and 2)
  * child threads detect closing of the pipes and stop themselves which also
    finishes child process

For the most part `ers2log` acts completely transparently and does not need
any change in a way how the application is being run.

Configuration
-------------

The only non-trivial part of `ers2log` is providing correct configuration for
it. The configuration is read using standard TDAQ `config` package, the
complication here is that `ers2log` is not a separate application with its own
associated configuration object or command line but a pre-loaded shared library.
To configure itself `log2ers` reads its configuration from a class with name
`Log2ERSConfig` (defined in `log2ers.schema.xml`), the ID of configuration
object is determined by environment variable `LOG2ERS_CONFIG_NAME` or
`TDAQ_APPLICATION_NAME`. To be able to access configuration environment
variable `TDAQ_DB` has to be defined as well. With these conventions it means
that it's easy to define `log2ers` configuration to match the ID of its
corresponding application, e.g.:

    <obj class="Application" id="CoralServer">
      ...
      <rel name="ProcessEnvironment" num="2">
        "Variable" "LD_PRELOAD_LOG2ERS"
        "Variable" "LOG2ERS_SPECIAL_TDAQ_ERS_ERROR"
      </rel>
    </obj>
    
    <!-- log2ers configuration used with CoralServer application -->
    <obj class="Log2ERSConfig" id="CoralServer">
      ...
    </obj>

`Log2ERSConfig` configuration is relatively simple - it defines a set of
matches for each output stream (and stream could use same set of matches).
Each possible match is described as a separate configuration object of a type
`Log2ERSLineParse`. It defines:
  * regular expression to match a line of output
  * associated message severity (ERS stream name), one of DEBUG, LOG, INFO,
    WARNING, ERROR, FATAL
  * "formatting" strings to extract various pieces of info from matched line -
    one for ERS message itself (in case output line contains something extra
    like timestamp of severity which does not need to appear in a message),
    and few others for optional package name, file name, function name and
    line number.

Regular expression and formatting strings follow C++11 regex library syntax
for ECMAScript. Formatting strings can be empty or can have some fixed text.

Here is an example of a simple configuration where each output stream has two
identical sets of matches:

    <obj class="Log2ERSLineParse" id="Match1">
     <attr name="Regexp" type="string">"(\d{4}-[A-Za-z]{3}-\d\d [0-9:,]+) ERROR (.*)"</attr>
     <attr name="Severity" type="enum">"ERROR"</attr>
     <attr name="Message" type="string">"$2 (original timestamp: $1)"</attr>
     <attr name="Package" type="string">"SomePackage"</attr>
     <attr name="FileName" type="string">"filename.cxx"</attr>
     <attr name="FunctionName" type="string">"FUN()"</attr>
     <attr name="LineNumber" type="string">"0"</attr>
    </obj>
    
    <obj class="Log2ERSLineParse" id="Match2">
     <attr name="Regexp" type="string">"ERROR (.*)"</attr>
     <attr name="Severity" type="enum">"ERROR"</attr>
     <attr name="Message" type="string">"$1"</attr>
     <attr name="Package" type="string">"SomePackage"</attr>
     <attr name="FileName" type="string">"filename.cxx"</attr>
     <attr name="FunctionName" type="string">"function()"</attr>
     <attr name="LineNumber" type="string">"0"</attr>
    </obj>
    
    <obj class="Log2ERSConfig" id="AppName">
     <rel name="StdoutParsers" num="2">
      "Log2ERSLineParse" "Match1"
      "Log2ERSLineParse" "Match2"
     </rel>
     <rel name="StderrParsers" num="2">
      "Log2ERSLineParse" "Match1"
      "Log2ERSLineParse" "Match2"
     </rel>
    </obj>

If `log2ers` encounters any error while reading configuration it will issue a
warning ERS message with the reason for failure and will continue to run
original application without redirecting its output (and without creating any
pipes or sub-process).

Limitations and quirks
----------------------

Because ERS messages are issued by the child process there is some context
information which is missing or incorrect. If application output does not
contain file name or line number it is not possible to guess that information.
Configuration in that case will have some empty or fixed values for those
fields and ERS message will contain those instead of actual info. If file name
and line number in the ERS messages are fixed then message throttling should
be disabled to avoid losing messages. 

Context information like process ID, thread ID, working directory will be
determined for child process and and not for original application process.

Timestamps of the reported ERS messages can be also unreliable or slightly
delayed, if the original output contains timestamps it is possible to include
those into the messages as shown in the configuration example.

If ERS is configured to log messages to standard output or error streams then
output for matching messages will be duplicated - one copy of a message will
be printed in original unmodified format, second copy will come from ERS
formatted according to ERS rules. IF this is confusing then one can modify
environment variables (`TDAQ_ERS_ERROR` and similar) to disable ERS
output to standard stream (and disable throttling too).

